public class Program2 {

    public static void main(String[] args) {
        int[] numbers = {1, 7, 22, 28, 31, 33, 43, 55, 87, 99};
        int numberToSearch = 28;
        int result = binarySearch(numbers, numberToSearch);
        if (result == -1)
            System.out.println("Элемент" + numberToSearch + " не найден");
        else
            System.out.println("Элемент " + numberToSearch + " найден по индексу " + result);
    }

    // переопределенный метод для двух входных параметров
    public static int binarySearch(int[] array, int element) {
        return recBinarySearchAlgorithm(array, 0, (array.length) - 1, element);
    }

    public static int recBinarySearchAlgorithm(int[] array, int left, int right, int element) {
        if (right >= left) {
            int mid = left + (right - left) / 2;
            if (array[mid] == element)
                return mid;
            if (array[mid] > element)
                return recBinarySearchAlgorithm(array, left, mid - 1, element);
            return recBinarySearchAlgorithm(array, mid + 1, right, element);
        }
        // если элемент не найден
        return -1;
    }
}

