public class Program1 {
    public static void main(String[] args) {

        double a = 128;
        if (checkPower(a) == 1) {
            int power = (int) ((Math.log(a)) / Math.log(2));
            System.out.println(", степень равна: " + power);
        }
    }

    public static int checkPower(double a) {
        if (a < 1) {
            System.out.println("Введённая степень меньше единицы");
            return -1;
        }
        if (a == 1) {
            System.out.print(true);
            return 1;
        }
        if (a > 1 && a < 2) {
            System.out.println(false);
            return 0;
        }
        return checkPower(a / 2);
    }
}
