public class Program3 {

    public static long calcFibonacci(long x, long previous, long next) {
        if (x <= 2) {
            return previous + next;
        }
        return calcFibonacci(x - 1, next, previous + next);
    }

    public static void main(String[] args) {
        int lastFibNumber = 100;

        for (int i = 1; i <= lastFibNumber; i++) {
            System.out.println(calcFibonacci(i, 0, 1));
        }
    }
}
