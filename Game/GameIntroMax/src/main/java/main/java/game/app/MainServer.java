package main.java.game.app;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import main.java.game.repositories.*;
import main.java.game.server.GameServer;
import main.java.game.services.GameService;
import main.java.game.services.GameServiceImpl;

import javax.sql.DataSource;

public class MainServer {

    public static void main(String[] args) {
        // создаем пул соединений с базой данных
        HikariConfig hikariConfig = new HikariConfig();
        // указываем данные для подключения
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/max");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("qwerty007");
        // указали сколько максимум может быть подключений
        hikariConfig.setMaximumPoolSize(20);
        // создали DataSource - источник данных
        DataSource dataSource = new HikariDataSource(hikariConfig);

        // создаем репозиторий, который использует этот источник данных
        GamesRepository gamesRepository = new GamesRepositoryJdbcImpl(dataSource);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        // создали сервис, который использует созданные выше репозитории
        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);
        // передали сервис объекту-серверу для нашей игры
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);


        // работа приложения с БД по socket connections c применением технологий Hikari Connection Pool, Maven, Lombok:
//        for (int gamesCount = 1; gamesCount <= 3; ) {
//            Scanner scanner = new Scanner(System.in);
//            System.out.println();
//            System.out.println("Игра началась! \nИгроки, введите Ваши имена.\nПервый игрок, имя, пожалуйста:");
//            String first = scanner.nextLine();
//            System.out.println("Второй игрок, представься!");
//            String second = scanner.nextLine();
//            Random random = new Random();
//
//            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.7", first, second);
//            String shooter = first;
//            String target = second;
//
//            int i = 0;
//            while (i < 10) {
//                System.out.println(shooter + " делайте выстрел в " + target);
//                scanner.nextLine();
//
//                int success = random.nextInt(2);
//
//                if (success == 0) {
//                    System.out.println("Успешно!");
//                    gameService.shot(gameId, shooter, target);
//                } else {
//                    System.out.println("Промах!");
//                }
//
//                String temp = shooter;
//                shooter = target;
//                target = temp;
//                i++;
//            }
//
//            StatisticDto statistic = gameService.finishGame(gameId);
//            System.out.println(statistic);
//            gamesCount++;
//        }
    }
}