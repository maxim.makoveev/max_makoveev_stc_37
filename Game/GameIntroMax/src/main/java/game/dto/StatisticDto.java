package main.java.game.dto;

import main.java.game.models.Game;
import main.java.game.models.Player;

// информация об игре
public class StatisticDto {
    private Game game;

    public StatisticDto(Game game) {
        this.game = game;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    @Override
    public String toString() {
        Player champ;
        //победил 1 игрок
        if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()) champ = game.getPlayerFirst();
        else //победил 2 игрок
            if (game.getPlayerFirstShotsCount() < game.getPlayerSecondShotsCount()) champ = game.getPlayerSecond();
                // боевая ничья
            else {
                return "Игра с ID = " + game.getId() + "\n" +
                        "Игрок 1: " + game.getPlayerFirst().getName() + ", попаданий - " + game.getPlayerFirstShotsCount() + ", всего очков - " + game.getPlayerFirst().getPoints() + "\n" +
                        "Игрок 2: " + game.getPlayerSecond().getName() + ", попаданий - " + game.getPlayerSecondShotsCount() + ", всего очков - " + game.getPlayerSecond().getPoints() + "\n" +
                        "Ничья" + "\n" +
                        "Игра длилась: " + game.getSecondsGameTimeAmount() + " секунд";
            }
        return "Игра с ID = " + game.getId() + "\n" +
                "Игрок 1: " + game.getPlayerFirst().getName() + ", попаданий - " + game.getPlayerFirstShotsCount() + ", всего очков - " + game.getPlayerFirst().getPoints() + "\n" +
                "Игрок 2: " + game.getPlayerSecond().getName() + ", попаданий - " + game.getPlayerSecondShotsCount() + ", всего очков - " + game.getPlayerSecond().getPoints() + "\n" +
                "Победа: " + champ.getName() + "\n" +
                "Игра длилась: " + game.getSecondsGameTimeAmount() + " секунд";
    }
}
