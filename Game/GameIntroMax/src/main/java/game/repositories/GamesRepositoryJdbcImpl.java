package main.java.game.repositories;

import main.java.game.models.Game;
import main.java.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Collection;


public class GamesRepositoryJdbcImpl implements GamesRepository {

    //language=SQL
    private static final String SQL_INSERT_GAME =
            "insert into game(datetime, playerfirst, playersecond, playerfirstshotscount, playersecondshotscount," +
                    "                 secondsgametimeamount) " +
                    "values (?, ?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_FIND_GAME_BY_ID =
            "select g.id as game_id," +
                    "       g.dateTime as game_datetime," +
                    "       p.id as playerfirst_id," +
                    "       p.ip as playerfirst_ip," +
                    "       p.name as playerfirst_name," +
                    "       p.points as playerfirst_points," +
                    "       p.maxWinsCount as playerfirst_maxwinscount," +
                    "       p.maxLosesCount as playerfirst_maxlosescount," +
                    "       p2.id as playersecond_id," +
                    "       p2.ip as playersecond_ip," +
                    "       p2.name as playersecond_name," +
                    "       p2.points as playersecond_points," +
                    "       p2.maxWinsCount  as playersecond_maxwinscount," +
                    "       p2.maxLosesCount as playersecond_maxlosescount," +
                    "       g.playerfirstshotscount as playerfirst_countshots," +
                    "       g.playersecondshotscount as playersecond_countshots," +
                    "       g.secondsGameTimeAmount as game_secondsgame " +
                    "from game as g" +
                    "         join player as p on g.playerfirst = p.id" +
                    "         join player p2 on g.playersecond = p2.id " +
                    "where g.id = ?";

    //language=SQl
    private static final String SQL_UPDATE_GAME_BY_ID =
            "update game " +
                    "set datetime               = ?," +
                    "    playerfirst         = ?," +
                    "    playersecond        = ?," +
                    "    playerfirstshotscount  = ?," +
                    "    playersecondshotscount = ?," +
                    "    secondsgametimeamount  =?" +
                    "where id = ?";

    static private final RowMapper<Player> playerFirstRowMapper = row -> Player.builder()
            .id(row.getLong("playerfirst_id"))
            .ip(row.getString("playerfirst_ip"))
            .name(row.getString("playerfirst_name"))
            .points(row.getInt("playerfirst_points"))
            .maxWinsCount(row.getInt("playerfirst_maxwinscount"))
            .maxLosesCount(row.getInt("playerfirst_maxlosescount"))
            .build();

    static private final RowMapper<Player> playerSecondRowMapper = row -> Player.builder()
            .id(row.getLong("playersecond_id"))
            .ip(row.getString("playersecond_ip"))
            .name(row.getString("playersecond_name"))
            .points(row.getInt("playersecond_points"))
            .maxWinsCount(row.getInt("playersecond_maxwinscount"))
            .maxLosesCount(row.getInt("playersecond_maxlosescount"))
            .build();

    static private final RowMapper<Game> gameRowMapper = row -> Game.builder()
            .id(row.getLong("game_id"))
            .dateTime(row.getTimestamp("game_datetime").toLocalDateTime())
            .playerFirst(playerFirstRowMapper.mapRow(row))
            .playerSecond(playerSecondRowMapper.mapRow(row))
            .playerFirstShotsCount(row.getInt("playerfirst_countshots"))
            .playerSecondShotsCount(row.getInt("playersecond_countshots"))
            .secondsGameTimeAmount(row.getLong("game_secondsgame"))
            .build();

    private DataSource dataSource;

    public GamesRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_GAME, Statement.RETURN_GENERATED_KEYS)) {
            statement.setObject(1, game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());

            int affectedRows = statement.executeUpdate();
            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                if (generatedId.next()) {
                    game.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Long gameId) {

        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_GAME_BY_ID)) {
            statement.setLong(1, gameId);
            try (ResultSet rows = statement.executeQuery()) {
                if (rows.next()) {
                    return gameRowMapper.mapRow(rows);
                }
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE_GAME_BY_ID)) {
            statement.setObject(1, game.getDateTime());
            statement.setLong(2, game.getPlayerFirst().getId());
            statement.setLong(3, game.getPlayerSecond().getId());
            statement.setInt(4, game.getPlayerFirstShotsCount());
            statement.setInt(5, game.getPlayerSecondShotsCount());
            statement.setLong(6, game.getSecondsGameTimeAmount());
            statement.setLong(7, game.getId());
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update data");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
