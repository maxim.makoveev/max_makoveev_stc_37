package main.java.game.repositories;

import main.java.game.models.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlayersRepositoryFilesImpl implements PlayersRepository {
    private String filename;
    private String sequencePlayerFileName;
    private Map<String, Player> players;

    public PlayersRepositoryFilesImpl(String filename, String sequencePlayerFileName) {
        this.filename = filename;
        this.sequencePlayerFileName = sequencePlayerFileName;
        players = new HashMap<>();
    }

    @Override
    public Player findByNickname(String nickname) {
        try {
            BufferedReader brdReader = new BufferedReader(new FileReader(filename));
            brdReader.lines().filter(string -> !string.isEmpty())
                    .map(string -> string.split("#"))
                    .anyMatch(name -> nickname.equals(name[2]));
            brdReader.close();
            return players.get(nickname);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {
        try {
            BufferedWriter brdWriter = new BufferedWriter(new FileWriter(filename, true));
            player.setId(generatePlayerId());
            brdWriter.write(player.getId() + "#" + player.getIp() + "#" + player.getName() + "#" + player.getPoints() + "#" + player.getMaxWinsCount() + "#" + player.getMaxLosesCount() + "\n");
            brdWriter.close();

        } catch (IOException e) {
            throw new IllegalStateException();
        }
        players.put(player.getName(), player);
    }

    private Long generatePlayerId() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequencePlayerFileName));
            String lastID = reader.readLine();
            long id = Long.parseLong(lastID);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequencePlayerFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();
            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        List<String> playerList = new ArrayList<>();
        try {
            BufferedReader brdReader = new BufferedReader(new FileReader(filename));
            String string;
            while ((string = brdReader.readLine()) != null) {
                playerList.add(string + "\n");
            }
            playerList.removeIf(players -> players.equals("" + "\n"));
            brdReader.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        String string = player.getId() + "#" + player.getIp() + "#" + player.getName() + "#" + player.getPoints() + "#" + player.getMaxWinsCount() + "#" + player.getMaxLosesCount() + "\n";
        for (String el : playerList) {
            String[] values = el.split("#");
            if (Long.parseLong(values[0]) == player.getId()) {
                playerList.set(playerList.indexOf(el), string);
            }
            try {
                BufferedWriter brWriter = new BufferedWriter(new FileWriter(filename));
                for (String i : playerList) {
                    brWriter.write(i);
                }
                brWriter.close();
            } catch (IOException e) {
                throw new IllegalStateException();
            }
        }
    }
}

