package main.java.game.repositories;

import main.java.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoryJdbcImpl implements ShotsRepository {
    //language=SQL
    private static final String SQL_INSERT_SHOT =
            "insert into shot(dateTime, game, shooter, target) " +
                    "values (?, ?, ?, ?)";

    private DataSource dataSource;

    public ShotsRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             // RETURN_GENERATED_KEYS означает, что запрос должен вернуть ключи, которые сгенерировала база данных
             // для текущего запроса
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT_SHOT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setObject(1, shot.getDateTime());
            statement.setLong(2, shot.getGame().getId());
            statement.setLong(3, shot.getShooter().getId());
            statement.setLong(4, shot.getTarget().getId());
            // сколько строк было обновлено
            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert data");
            }
            // ключи, которые база сгенерировала сама для этого запроса
            try (ResultSet generatedId = statement.getGeneratedKeys()) {
                // проверяем, сгенерировала ли база что-либо?
                if (generatedId.next()) {
                    // получаем ключ, который сгенерировала база ДЛЯ текущего запроса
                    shot.setId(generatedId.getLong("id"));
                } else {
                    throw new SQLException("Can't retrieve id");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }
}
