package main.java.game.repositories;

import main.java.game.models.Shot;

public interface ShotsRepository {
    void save(Shot shot);
}
