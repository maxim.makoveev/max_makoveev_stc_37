package main.java.game.repositories;

import main.java.game.models.Shot;

import java.io.*;

public class ShotsRepositoryFileImpl implements ShotsRepository {
    private String dbfilename;
    private String sequenceFileName;

    public ShotsRepositoryFileImpl(String dbfilename, String sequenceFileName) {
        this.dbfilename = dbfilename;
        this.sequenceFileName = sequenceFileName;
    }

    @Override
    public void save(Shot shot) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(dbfilename, true));
            shot.setId(generatePlayerId());
            writer.write(shot.getId() + "#" +shot.getDateTime().toString() + "#" +
                    shot.getGame().getId() + "#" +shot.getShooter().getName() +"#" + shot.getTarget().getName() + "\n");
            writer.close();
        } catch (IOException e) {
            throw new IllegalStateException();
        }
    }

    private Long generatePlayerId(){
        try {
            BufferedReader reader = new BufferedReader(new FileReader(sequenceFileName));
            String lastGeneratedIdAsString = reader.readLine();
            Long id = Long.parseLong(lastGeneratedIdAsString);
            reader.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter(sequenceFileName));
            writer.write(String.valueOf(id + 1));
            writer.close();
            return id;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
