package main.java.game.repositories;

import main.java.game.models.Game;

public interface GamesRepository {

    void save(Game game);

    Game findById(Long gameId);

    void update(Game game);
}
