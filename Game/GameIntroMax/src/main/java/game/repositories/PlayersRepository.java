package main.java.game.repositories;

import main.java.game.models.Player;

public interface PlayersRepository {
    Player findByNickname(String nickname);

    void save(Player player);

    void update(Player player);
}
