package com.company;

public class NumbersAndStringProcessor {
    private final int[] numbers;
    private final String[] strings;

    public NumbersAndStringProcessor(int[] numbers, String[] strings) {
        this.numbers = numbers;
        this.strings = strings;
    }

    public int[] process(NumbersProcess process) {
        int[] processedArray = new int[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            processedArray[i] = process.process(numbers[i]);
        }
        return processedArray;
    }

    public String[] process(StringProcess process) {
        String[] processedArray = new String[strings.length];
        for (int i = 0; i < strings.length; i++) {
            processedArray[i] = process.process(strings[i]);
        }
        return processedArray;
    }
}
