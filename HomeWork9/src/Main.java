package com.company;

import java.util.Arrays;
import java.util.Locale;

public class Main {

    public static void main(String[] args) {
        NumbersAndStringProcessor processor = new NumbersAndStringProcessor(new int[]{301, 57, 84, 9, 1102, 42},
                new String[]{"00000Hel0lo", "Where's the mo0ney, Lebowski?", "Adios!"});

        NumbersProcess reverse = numberR -> {
            StringBuilder reversedNumber = new StringBuilder(String.valueOf(numberR));
            reversedNumber.reverse();
            return Integer.parseInt(reversedNumber.toString());
        };

        NumbersProcess removeZeros = numberRZ -> {
            if (numberRZ != 0) {
                return Integer.parseInt(Integer.toString(numberRZ).replace("0", ""));
            }
            return 0;
        };

        NumbersProcess castToEvenFloor = numberC -> {
            int processed = 0;
            int multi = 1;
            while (numberC != 0) {
                int evenCheck = numberC % 10;
                processed += ((evenCheck % 2 == 0 ? evenCheck : evenCheck - 1) * multi);
                multi *= 10;
                numberC /= 10;
            }
            return processed;
        };

        StringProcess reverseString = StringR -> {
            StringBuilder reversedNumber = new StringBuilder(StringR);
            reversedNumber.reverse();

            return reversedNumber.toString();
        };

        StringProcess removeZerosString = StringRZ -> {
            String find = "0";
            return StringRZ.replaceAll(find, "");
        };

        StringProcess toUpperCase = StringUC -> StringUC.toUpperCase();

        System.out.println(Arrays.toString(processor.process(reverse)));
        System.out.println(Arrays.toString(processor.process(removeZeros)));
        System.out.println(Arrays.toString(processor.process(castToEvenFloor)));
        System.out.println("Strings: ");
        System.out.println(Arrays.toString(processor.process(reverseString)));
        System.out.println(Arrays.toString(processor.process(removeZerosString)));
        System.out.println(Arrays.toString(processor.process(toUpperCase)));
    }
}
