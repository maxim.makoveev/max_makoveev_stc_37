public class User {
    private String firstName;
    private String lastName;
    private int age;
    private boolean isWorker;

    private User() {

    }

    public static Builder builder() {
        return new User().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder FirstName(String firstName) {
            User.this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            User.this.lastName = lastName;
            return this;
        }

        public Builder setAge(int age) {
            User.this.age = age;
            return this;
        }

        public Builder setIsWorker(boolean isWorker) {
            User.this.isWorker = isWorker;
            return this;
        }

        public User build() {
            User user = new User();
            user.firstName = User.this.firstName;
            user.lastName = User.this.lastName;
            user.age = User.this.age;
            user.isWorker = User.this.isWorker;
            System.out.printf("%s %s %d %s %n", user.firstName, user.lastName,
                    user.age, user.isWorker);
            return user;
        }
    }
}
