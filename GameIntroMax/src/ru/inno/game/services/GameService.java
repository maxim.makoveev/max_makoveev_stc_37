package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;

public interface GameService {

    /**
     * @param firstIp адрес, с которого зашел первый игрок
     * @param secondIp ..зашел второй игрок
     * @param firstPlayerNickname имя первого игрока
     * @param secondPlayerNickname имя второго игрока
     * @return идентификатор игры
     */
    Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    /**
     * Фиксирует выстрел игроков (попавшие)
     * @param gameId идентификатор игры
     * @param shooterNickname имя 1 игрока
     * @param targetNickname имя 2 игрока
     */
    void shot(Long gameId, String shooterNickname, String targetNickname);

    StatisticDto finishGame(Long gameId);
}
