package ru.inno.game.services;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repositories.GamesRepository;
import ru.inno.game.repositories.PlayersRepository;
import ru.inno.game.repositories.ShotsRepository;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class GameServiceImpl implements GameService {

    private PlayersRepository playersRepository;

    private GamesRepository gamesRepository;

    private ShotsRepository shotsRepository;

    public GameServiceImpl(PlayersRepository playersRepository, GamesRepository gamesRepository, ShotsRepository shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Long startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили инфо об обоих игроках
        Player first = checkIfExists(firstIp, firstPlayerNickname);
        Player second = checkIfExists(secondIp, secondPlayerNickname);
        // создали игру
        Game game = new Game(LocalDateTime.now(), first, second, 0, 0, 0L);
        // сохранили игру в репозитории
        gamesRepository.save(game);
        return game.getId();
    }

    // проверили если ли они у нас
    private Player checkIfExists(String ip, String nickname) {
        Player player = playersRepository.findByNickname(nickname);
        // если нет 1 игрока под таким именем
        if (player == null) {
            // создать игрока
            player = new Player(ip, nickname, 0, 0, 0);
            // сохраняем его в репозитории
            playersRepository.save(player);
        } else {
            // если такой игрок был - обновляем у него ip адрес
            player.setIp(ip);
            playersRepository.update(player);
        }
        return player;
    }

    @Override
    public void shot(Long gameId, String shooterNickname, String targetNickname) {
        // получаем того, кто стрелял, из репозитория
        Player shooter = playersRepository.findByNickname(shooterNickname);
        // получаем того, в кого стреляли, из репозитория
        Player target = playersRepository.findByNickname(targetNickname);
        // получаем игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
        Shot shot = new Shot(LocalDateTime.now(), game, shooter, target);
        // увеличиваем очки у стреляющего
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелявший - первый игрок
        if (game.getPlayerFirst().getName().equals(shooterNickname)) {
            //сохраняем информацию о выстреле в игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }

        if (game.getPlayerSecond().getName().equals(shooterNickname)) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        //обновляем данные по стреляющему, по игре, сохраняем выстрел
        playersRepository.update(shooter);
        gamesRepository.update(game);
        shotsRepository.save(shot);
    }

    @Override
    public StatisticDto finishGame(Long gameId) {
        Game game = gamesRepository.findById(gameId);
        StatisticDto statistic = new StatisticDto(game);

        if (game.getPlayerFirstShotsCount() > game.getPlayerSecondShotsCount()) {
            game.getPlayerFirst().setMaxWinsCount(game.getPlayerFirst().getMaxWinsCount() + 1);
            game.getPlayerSecond().setMaxLosesCount(game.getPlayerSecond().getMaxLosesCount() + 1);
        } else {
            game.getPlayerFirst().setMaxLosesCount(game.getPlayerFirst().getMaxLosesCount() + 1);
            game.getPlayerSecond().setMaxWinsCount(game.getPlayerSecond().getMaxWinsCount() + 1);
        }

        playersRepository.update(game.getPlayerFirst());
        playersRepository.update(game.getPlayerSecond());
        // вычисляется длительность игры
        long time = Duration.between(LocalTime.now(), game.getDateTime()).getSeconds();
        game.setSecondsGameTimeAmount(Math.abs(time));
        gamesRepository.update(game);
        return statistic;
    }
}
