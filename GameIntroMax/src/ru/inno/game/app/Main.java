package ru.inno.game.app;

import ru.inno.game.dto.StatisticDto;
import ru.inno.game.repositories.*;
import ru.inno.game.services.GameService;
import ru.inno.game.services.GameServiceImpl;
import ru.inno.game.utils.CustomDataSource;

import javax.sql.DataSource;
import java.util.Random;
import java.util.Scanner;

public class Main {

    private static final String JDBC_URL = "jdbc:postgresql://localhost:5432/max";
    private static final String JDBC_USER = "postgres";
    private static final String JDBC_PASSWORD = "qwerty007";

    public static void main(String[] args) {
//        PlayersRepository playersRepository = new PlayersRepositoryFilesImpl("players_db.txt", "players_sequence.txt"); // если через HashMap: new PlayersRepositoryMapImpl()
//        GamesRepository gamesRepository = new GamesRepositoryListImpl();
//        ShotsRepository shotsRepository = new ShotsRepositoryFileImpl("shots_db.txt", "shots_sequence.txt"); // реализация через список: new ShotsRepositorylistImpl
//        GameService gameService = new GameServiceImpl(playersRepository, gamesRepository, shotsRepository);

        DataSource dataSource = new CustomDataSource(JDBC_USER, JDBC_PASSWORD, JDBC_URL);
        PlayersRepository playersRepository = new PlayersRepositoryJdbcImpl(dataSource);
        GamesRepository gameRepository = new GamesRepositoryJdbcImpl(dataSource);
        ShotsRepository shotsRepository = new ShotsRepositoryJdbcImpl(dataSource);
        GameService gameService = new GameServiceImpl(playersRepository, gameRepository, shotsRepository);

        for (int gamesCount = 1; gamesCount <= 3; ) {
            Scanner scanner = new Scanner(System.in);
            System.out.println();
            System.out.println("Игра началась! \nИгроки, введите Ваши имена.\nПервый игрок, имя, пожалуйста:");
            String first = scanner.nextLine();
            System.out.println("Второй игрок, представься!");
            String second = scanner.nextLine();
            Random random = new Random();

            Long gameId = gameService.startGame("127.0.0.1", "127.0.0.7", first, second);
            String shooter = first;
            String target = second;
            int i = 0;
            while (i < 10) {
                System.out.println(shooter + " делайте выстрел в " + target);
                scanner.nextLine();

                int success = random.nextInt(2);

                if (success == 0) {
                    System.out.println("Успешно!");
                    gameService.shot(gameId, shooter, target);
                } else {
                    System.out.println("Промах!");
                }

                String temp = shooter;
                shooter = target;
                target = temp;
                i++;
            }

            StatisticDto statistic = gameService.finishGame(gameId);
            System.out.println(statistic);
            gamesCount++;
        }
    }
}