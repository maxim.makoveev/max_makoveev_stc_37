create table Player (
    id bigserial primary key,
    ip varchar(20),
    name varchar(20),
    points integer,
    maxWinsCount integer,
    maxLosesCount integer
);

create table Game (
    id bigserial primary key,
    dateTime timestamp,
    playerFirst integer,
    playerSecond integer,
    playerFirstShotsCount integer,
    playerSecondShotsCount integer,
    secondsGameTimeAmount integer,
    foreign key (playerFirst) references Player(id),
    foreign key (playerSecond) references  Player(id)
);

create table Shot (
    id bigserial primary key,
    dateTime timestamp,
    game integer,
    shooter integer,
    target integer,
    foreign key (game) references Game(id),
    foreign key (shooter) references Player(id),
    foreign key (target) references Player(id));
