package com.company;

import java.util.Scanner;

public class Program3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int composition = 1;

        // начало цикла
        while (scanner.hasNextLine()) {
            String input = scanner.next();

            // выход из цикла при нуле
            if (input.equals("0")) {
                break;
            }

            // подсчет суммы знаков числа
            int sum = input.chars().map(c -> Character.digit(c, 10)).sum();
//            System.out.println(sum);

            // проверка на простоту
            for (int i = 2; i <= sum / 2; ++i) {
//              System.out.println("Prime");
                if ((sum % i == 0)) {
//                    System.out.println("Composite");
                } else composition = Integer.parseInt(input) * composition;
                break;
            }
        }
        System.out.println(composition);
    }
}
