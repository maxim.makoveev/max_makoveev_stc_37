package com.company;

public class Program2 {

    public static void main(String[] args) {
        int number = 12345;
        String binaryNumber = Integer.toBinaryString(number);
        System.out.println(binaryNumber);

        // если нужно вывести двоичное представление числа с лидирующими нулями:
//        String binaryWithLeadingZeros = String.format("%16s", Integer.toBinaryString(number)).replace(' ', '0');
//        System.out.println(binaryWithLeadingZeros);
    }
}
