package com.company;

public class Program {

    public static void main(String[] args) {
        int number = 12345;
        int digitsSum = 0;

        while (number != 0) {
            digitsSum += number % 10;
            number = number / 10;
        }
        System.out.println(digitsSum);
    }
}
