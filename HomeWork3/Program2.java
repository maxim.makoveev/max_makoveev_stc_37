
import java.util.Scanner;

public class Program2 {

    // эталонное значение интеграла 5..10 для сравнения разницы
    public static final double VALUE = 291.67;

    // функция взятия степени
    public static double f(double x) {
        return x * x;
    }

    // а - начало интеграла, b - конец интеграла, n - количество разбиений
    public static double calcIntegral(double a, double b, int n) {
        double h = (b - a) / n;
        double integral = f(a) + f(b);
        int k;

        for (int i = 1; i <= n - 1; i++) {
            k = 2 + 2 * (i % 2);
            integral += k * f(a + i * h);
        }
        integral *= h / 3;

        return integral;
    }

    // steps - количество проходов в цикле, eps - значение интеграла, ys - разница с эталонным значением
    public static void calcIntegralsOnSteps(double a, double b, int[] steps, double[] eps, double[] ys) {
        for (int i = 0; i < steps.length; i++) {
            ys[i] = calcIntegral(a, b, steps[i]);
            eps[i] = ys[i] - VALUE;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();
        // погрешность
        double observationError = 0.0001;
        // начальный шаг для поиска
        int startingStepsForSearch = 4;
        // итератор
        int j = 0;
        // разница между значениями
        double diff;

        int[] steps = {3, 5, 6, 9, 21, 53, 1000};
        double[] eps = new double[steps.length];
        double[] ys = new double[steps.length];

        calcIntegralsOnSteps(a, b, steps, eps, ys);

        System.out.printf("%7s| %18s| %7s| \n", "N", "y", "eps");
        System.out.println("     ---------------------------------");

        for (int i = 0; i < steps.length; i++) {
            System.out.printf("%7d| %3.14f| %2.3f| \n", steps[i], ys[i], eps[i]);
        }
        System.out.println();
        // вычисляется приемлимое количество шагов для заданной погрешности
        do {
            j++;
            diff = Math.abs(calcIntegral(a, b, startingStepsForSearch * j) - calcIntegral(a, b,
                    startingStepsForSearch * (j + 1)));
        } while (diff > observationError);

        System.out.print("Искомое количество итераций (N) для заданной погрешности: "
                + startingStepsForSearch * (j + 1)
                + "\nЗначение: " + calcIntegral(a, b, startingStepsForSearch * (j + 1)));
    }
}


