import java.util.Arrays;
import java.util.Scanner;

public class Program1 {


    public static void main(String[] args) {

        System.out.println("1: " + sumArray(new int[]{11, 232, 245, -666}));
        System.out.print("2: ");
        System.out.println(reverseArray());
        System.out.println("3: " + countAverageSumArray(new double[]{123, 23, 25, 7}));
        System.out.println("4: " + swapMinMaxInArray(new int[]{52, 42, 835, 67, 88, 45, 9, 44}));
        System.out.println("5: " + bubbleSortArray(new int[]{8923, 234, 5, 3411, 987, 32, 4}));
        System.out.println("6: " + transformArrayToNumber(new int[]{1, 3, 6, 777, 2}));

    }

    public static int sumArray(int[] sum) {
        int arraySum = 0;
        for (int element : sum) {
            arraySum += element;
        }
        return arraySum;
    }

    public static String reverseArray() {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int arraySize = array.length;

        int i = 0;
        while (i < n) {
            array[i] = scanner.nextInt();
            i++;
        }
        for (int j = 0; j < arraySize / 2; j++) {
            int temp = array[j];
            array[j] = array[arraySize - 1 - j];
            array[arraySize - 1 - j] = temp;
        }
        return Arrays.toString(array);
    }

    public static double countAverageSumArray(double[] average) {
        double arrayAverage = 0;

        for (double num : average) {
            arrayAverage = arrayAverage + num;
        }
        return arrayAverage / average.length;
    }

    public static String swapMinMaxInArray(int[] swap) {
        int min = 0;
        int max = 0;
        int temp;

        for (int j = 0; j < swap.length; j++) {
            if (swap[min] > swap[j]) min = j;
            if (swap[max] < swap[j]) max = j;
        }

        temp = swap[min];
        swap[min] = swap[max];
        swap[max] = temp;

        return "Итоговый массив: " + Arrays.toString(swap);
    }

    public static String bubbleSortArray(int[] sort) {
        boolean isSorted = false;
        int tempArray;


        while (!isSorted) {
            isSorted = true;
            for (int j = 0; j < sort.length - 1; j++) {
                if (sort[j] > sort[j + 1]) {
                    isSorted = false;

                    tempArray = sort[j];
                    sort[j] = sort[j + 1];
                    sort[j + 1] = tempArray;
                }
            }
        }
        return "Отсортированный \"пузырьковым\" методом массив: " + Arrays.toString(sort);
    }

    public static int transformArrayToNumber(int[] transformation) {
        StringBuilder sb = new StringBuilder();
        for (int j : transformation) sb.append(j);

        return Integer.parseInt(sb.toString());
    }
}
