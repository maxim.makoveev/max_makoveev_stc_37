public class Main {

    public static void main(String[] args) {
        // создал список
        InnoList list = new InnoArrayList();

        testNumbers(list);

        System.out.println(list.get(0)); // 7
        System.out.println(list.get(3)); // 12
        System.out.println(list.get(7)); // 100

        System.out.println(list.contains(8));
        list.insert(2, 777); //777 вместо 10
        list.addToBegin(222);
        System.out.println("Размер ArrayList после добавления элементов:" + list.size());
        list.removeByIndex(4); // убирается 12 по индексух[4]
        list.remove(20); // убирается значение 100
        System.out.println("Итоговый размер ArrayList:" + list.size());

        InnoIterator iterator = list.iterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next() + " ");
        }

        InnoList list2 = new InnoLinkedList();
        System.out.println("__________________");
        list2.add(345);

        testNumbers(list2);
        System.out.println(list2.contains(20));
        list2.remove(8);
        System.out.println(list2.get(6)); // берет value -77 по индексу 6
        list2.insert(1,9); // вставляет 9 на место 7
        list2.addToBegin(333);
        list2.removeByIndex(3); // удаляет 10 оп индексу

        InnoIterator iterator2 = list2.iterator();

        while (iterator2.hasNext()) {
            System.out.println(iterator2.next() + " ");
        }
        if (!iterator2.hasNext()) System.out.println(list2.get(list2.size() - 1));
    }

    private static void testNumbers(InnoList list2) {
        list2.add(7);
        list2.add(8);
        list2.add(10);
        list2.add(12);
        list2.add(15);
        list2.add(20);
        list2.add(-77);
        list2.add(100);
    }
}
