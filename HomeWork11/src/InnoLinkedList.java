
// Плюсы - неограниченный размер (ограничен VM), быстрое добавление/удаление в начало/конец
// Минусы - медленный доступ по  индексу
public class InnoLinkedList implements InnoList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }

    private Node first;
    private Node last;

    private int count;

    // get(7)
    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            // начинаем с первого элемента
            Node current = first;
            // отсчитываем элементы с начала списка пока не дойдем до элемента с нужной позицией
            for (int i = 0; i < index; i++) {
                // переходим к следующему
                current = current.next; // семь раз сделаю next
            }
            // возвращаем значение
            return current.value;
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            current.value = element;
        }
    }

    @Override
    public void addToBegin(int element) {
        Node newNode = new Node(element);
        newNode.next = first;
        first = newNode;
        count++;
    }

    @Override
    public void removeByIndex(int index) {
        Node current = first;
        if (index >= 0 && index < count) {
            for (int i = 0; i < index; i++) {
                current = current.next;
            }
            while (current.next.next != null) {
                current.value = current.next.value;
                current = current.next;
            }
            current.value = current.next.value;
            current.next = null;
            count--;
        }
    }

//    @Override
//    public void add(int element) {
//        // новый узел для нового элемента
//        Node newNode = new Node(element);
//        // если список пустой
//        if (first == null) {
//            // новый элемент списка и есть самый первый
//            first = newNode;
//        } else {
//            // если элементы в списке уже есть, необходимо добраться до последнего
//            Node current = first;
//            // пока не дошли до узла, после которого ничего нет
//            while (current.next != null) {
//                // переходим к следующему узлу
//                current = current.next;
//            }
//            // дошли по последнего узла
//            // теперь новый узел - самый последний (следующий после предыдущего последнего)
//            current.next = newNode;
//        }
//        count++;
//    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
        } else {
            // следующий после последнего - новый узел
            last.next = newNode;

        }
        // новый узел - последний
        last = newNode;
        count++;
    }

    @Override
    public void remove(int element) {
        Node current = first;
        for (int i = 0; i < count; i++) {
            if (current.value == element) {
                while (current.next.next != null) {
                    current.value = current.next.value;
                    current = current.next;
                }
                current.value = current.next.value;
                current.next = null;
                count--;
                break;
            }
            current = current.next;
        }
    }

    @Override
    public boolean contains(int element) {
        Node current = first;
        for (int i = 0; i < count; i++) {
            if (current.value == element) return true;
            current = current.next;
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        return new InnoLinkedListIterator();
    }

    private class InnoLinkedListIterator implements InnoIterator {

        // ссылка на текущий узел итератора
        private Node current;

        InnoLinkedListIterator() {
            this.current = first;
        }

        @Override
        public int next() {
            int nextValue = current.value;
            // сдвигаем указатель на следующий узел
            current = current.next;
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если следующего узла нет - не идем дальше
            return current.next != null;
        }
    }
}
