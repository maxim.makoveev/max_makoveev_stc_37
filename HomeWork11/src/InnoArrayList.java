import java.util.stream.IntStream;

public class InnoArrayList implements InnoList {

    private static final int DEFAULT_SIZE = 10;

    private int[] elements;

    private int count;

    public InnoArrayList() {
        this.elements = new int[DEFAULT_SIZE];
        this.count = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            return -1;
        }
    }

    @Override
    public void insert(int index, int element) {
        if (count == elements.length) {
            resize();
        }
        // вставка элемента по индексу
        elements[index] = element;
    }

    @Override
    public void addToBegin(int element) {
        int[] newElements = new int[elements.length + elements.length / 2];
        newElements[0] = element;
        count++;
        //сдвиг списка вправо на 1 позицию
        if (count - 1 >= 0) System.arraycopy(elements, 0, newElements, 1, count - 1);
        this.elements = newElements;
    }

    @Override
    public void removeByIndex(int index) {
        if (count - index >= 0) System.arraycopy(elements, index + 1, elements, index, count - index);
        elements[count] = 0;
        count--;
    }

    @Override
    public void add(int element) {
        // если список переполнен
        if (count == elements.length) {
            resize();
        }
        elements[count] = element;
        count++;
    }

    private void resize() {
        // создаем новый массив в полтора раза больший
        int newElements[] = new int[elements.length + elements.length / 2];
        // копируем из старого массива все элементы в новый
        if (count >= 0) System.arraycopy(elements, 0, newElements, 0, count);
        // устанавливаем ссылку на новый массив
        this.elements = newElements;
    }

    @Override
    public void remove(int element) {
        for (int i = 0; i < count; i++) {
            if (elements[i] == element) {
                int bound = count;
                for (int y = i; y < bound; y++) {
                    elements[y] = elements[y + 1];
                }
                this.count--;
            }
        }
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < count; i++)
            if (elements[i] == element)
                return true;
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public InnoIterator iterator() {
        // возвращаем новый экземпляр итератора
        return new InnoArrayListIterator();
    }

    // внутренний класс позволяет инкапсулировать логику одного класса внутри класса
    private class InnoArrayListIterator implements InnoIterator {
        // текущая позиция итератора
        private int currentPosition;

        @Override
        public int next() {
            // берем значение под текущей позицией итератора
            int nextValue = elements[currentPosition];
            // увеличиваем позицию итератора
            currentPosition++;
            // возвращаем значение
            return nextValue;
        }

        @Override
        public boolean hasNext() {
            // если текущая позиция не перевалила за общее количество элементов - можно дальше
            return currentPosition < count;
        }
    }
}
