import java.util.Arrays;
import java.util.Scanner;

public class fifthProgram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        boolean isSorted = false;
        int tempArray;

        int i = 0;
        while (i < n) {
            array[i] = scanner.nextInt();
            i++;
        }

        while(!isSorted) {
            isSorted = true;
            for (int j = 0; j < array.length-1; j++) {
                if(array[j] > array[j+1]){
                    isSorted = false;

                    tempArray = array[j];
                    array[j] = array[j+1];
                    array[j+1] = tempArray;
                }
            }
        }
        System.out.println("Отсортированный \"пузырьковым\" методом массив: " + Arrays.toString(array));
    }
}
