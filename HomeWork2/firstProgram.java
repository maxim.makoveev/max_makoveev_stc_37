import java.util.Scanner;

public class firstProgram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        int arraySum = 0;

        int i = 0;
        while (i < n) {
            array[i] = scanner.nextInt();
            i++;
        }
        for (int num : array) {
            arraySum = arraySum + num;
        }

        System.out.println(arraySum);
    }
}

