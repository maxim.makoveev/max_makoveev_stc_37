import java.util.Scanner;
import java.util.Arrays;

public class forthProgram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] array = new int[n];
        int min = 0;
        int max = 0;
        int temp;

        int i = 0;
        while (i < n) {
            array[i] = scanner.nextInt();
            i++;
        }

        for (int j = 0; j < array.length; j++) {
            if (array[min] > array[j]) min = j;
            if (array[max] < array[j]) max = j;
        }
        System.out.println("Исходный массив: " + Arrays.toString(array));

        temp = array[min];
        array[min] = array[max];
        array[max] = temp;

        System.out.print("Итоговый массив: " + Arrays.toString(array));
    }
}
