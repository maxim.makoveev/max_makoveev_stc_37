import java.util.Arrays;
import java.util.Scanner;

public class secondProgram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        int arraySize = array.length;

        int i = 0;
        while (i < n) {
            array[i] = scanner.nextInt();
            i++;
        }
        for (int j = 0; j < arraySize / 2; j++) {
            int temp = array[j];
            array[j] = array[arraySize - 1 - j];
            array[arraySize - 1 - j] = temp;
        }
        System.out.println(Arrays.toString(array));
    }
}
