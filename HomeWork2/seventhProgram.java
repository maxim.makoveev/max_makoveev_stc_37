public class seventhProgram {
    public static void main(String[] args) {

        int rows = 6;
        int colums = 5;

        // начальное число заполнения массива
        int startingNumber = 1;

        int[][] array = new int[rows][colums];

        // спиральное заполнение массива по часовой стрелке
        for (int j = 0; j < colums; j++) {
            array[0][j] = startingNumber;
            startingNumber++;
        }
        for (int i = 1; i < rows; i++) {
            array[i][colums - 1] = startingNumber;
            startingNumber++;
        }
        for (int j = colums - 2; j >= 0; j--) {
            array[rows - 1][j] = startingNumber;
            startingNumber++;
        }
        for (int i = rows - 2; i > 0; i--) {
            array[i][0] = startingNumber;
            startingNumber++;
        }

        // начальные координаты ячейки для следующего заполнения
        int a = 1;
        int b = 1;

        while (startingNumber < rows * colums) {

            // движение по матрице вправо
            while (array[a][b + 1] == 0) {
                array[a][b] = startingNumber;
                startingNumber++;
                b++;
            }

            // движение по матрице вниз
            while (array[a + 1][b] == 0) {
                array[a][b] = startingNumber;
                startingNumber++;
                a++;
            }

            // движение по матрице влево
            while (array[a][b - 1] == 0) {
                array[a][b] = startingNumber;
                startingNumber++;
                b--;
            }

            // движение по матрице вверх
            while (array[a - 1][b] == 0) {
                array[a][b] = startingNumber;
                startingNumber++;
                a--;
            }
        }
        // исключение последней незаполненной ячейки в центре
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < colums; y++) {
                if (array[x][y] == 0) {
                    array[x][y] = startingNumber;
                }
            }
        }
        // вывод массива в консоль
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < colums; y++) {
                if (array[x][y] < 10) {

                    // выравнивание цифр по сетке в матрице
                    System.out.print(array[x][y] + "   ");
                } else {
                    System.out.print(array[x][y] + "  ");
                }
            }
            System.out.println("   ");
        }
    }
}
