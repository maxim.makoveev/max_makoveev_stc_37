import java.util.Scanner;

public class thirdProgram {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int array[] = new int[n];
        double arrayAverage = 0;
        int i = 0;
        while (i < n) {
            array[i] = scanner.nextInt();
            i++;
        }
        for (int num : array) {
            arrayAverage = arrayAverage + num;
        }
        arrayAverage = arrayAverage / array.length;

        System.out.println(arrayAverage);
    }
}
