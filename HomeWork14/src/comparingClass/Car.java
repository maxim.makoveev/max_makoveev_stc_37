package comparingClass;

import java.util.Objects;

public class Car {
    private final String carPlate;
    private final String carModel;
    private final String colour;
    private final int mileage;
    private final int price;

    public Car(String carPlate, String carModel, String colour, int mileage, int price) {
        this.carPlate = carPlate;
        this.carModel = carModel;
        this.colour = colour;
        this.mileage = mileage;
        this.price = price;
    }

    public String getCarPlate() {
        return carPlate;
    }

    public String getCarModel() {
        return carModel;
    }

    public String getColour() {
        return colour;
    }

    public int getMileage() {
        return mileage;
    }

    public int getPrice() {
        return price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return mileage == car.mileage && price == car.price && Objects.equals(carPlate, car.carPlate) && Objects.equals(carModel, car.carModel) && Objects.equals(colour, car.colour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(carPlate, carModel, colour, mileage, price);
    }
}
