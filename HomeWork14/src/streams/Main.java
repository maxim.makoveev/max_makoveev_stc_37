package streams;

import comparingClass.Car;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Car car0 = new Car("e466ex98", "VW Touran", "red", 123_000, 1_600_000);
        Car car1 = new Car("e467ex98", "VW Beetle", "pink", 64_000, 2_000_000);
        Car car2 = new Car("e468ex98", "VW Tuareg", "black", 12_500, 4_700_000);
        Car car3 = new Car("e469ex98", "VW Polo", "white", 32_000, 787_000);
        Car car4 = new Car("e470ex98", "VW Passat B5", "brown", 222_000, 765_000);
        Car car5 = new Car("e471ex98", "VW Tiguan", "black", 1_000, 2_800_000);
        Car car6 = new Car("e472ex98", "Toyota Camry", "green", 0, 2_900_101);
        Car car7 = new Car("e463ex98", "VW Polo", "yellow", 404_000, 123_000);
        Car car8 = new Car("e464ex98", "Toyota Camry", "white", 0, 2_500_000);
        Car car9 = new Car("e465ex98", "VW Polo", "black", 66_600, 777_000);

        List<Car> cars = new ArrayList<>(Arrays.asList(car0, car1, car2, car3, car4, car5, car6, car7, car8, car9));

        // номера машин в чёрном цвете или без пробега
        System.out.println("1/ номера машин в чёрном цвете или без пробега:");
        List<String> plates =
                cars.stream()
                        .filter(car -> car.getColour().equals("black") || car.getMileage() == 0)
                        .map(Car::getCarPlate)
                        .collect(Collectors.toList());
        System.out.println(plates);

        // количество уникальных моделей, где цена в диапазоне [700_000...800_000]
        System.out.println("2/ количество уникальных моделей, где цена в диапазоне [700_000...800_000]:");
        List<String> quantity =
                cars.stream()
                        .filter(car -> car.getPrice() > 700_000 && car.getPrice() < 800_000)
                        .map(Car::getCarModel)
                        .distinct()
                        .collect(Collectors.toList());
        System.out.println(quantity.size());

        // цвет самого бюджетного авто
        System.out.println("3/ цвет самого бюджетного авто");
        List<String> colour =
                cars.stream()
                        .sorted(Comparator.comparing(Car::getPrice))
                        .map(Car::getColour)
                        .collect(Collectors.toList());
        System.out.println(colour.get(0));

        // средняя стоимость Camry
        System.out.println("4/ средняя стоимость Camry");
        double averageCamryPrice =
                cars.stream()
                        .filter(car -> car.getCarModel().equals("Toyota Camry"))
                        .mapToDouble(Car::getPrice)
                        .average()
                        .orElse(-1);
        System.out.println(averageCamryPrice);
    }
}
