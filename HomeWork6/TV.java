public class TV {
    private final Channel[] channels;
    // поле Remote RemoteController remoteController убрано!

    public TV(Channel[] channels) {
        this.channels = channels;
    }

    // подключение связи "TV <-> RemoteController"
    public void connectRemote(RemoteController remoteController) {
        remoteController.setTv(this);
    }

    // метод обращения к каналу по индексу с возвратом случ. программы
    public Program switchChannel(int input) {
        System.out.print(channels[--input].getChannelName());
        return this.channels[input].showRandomizedProgram();
    }

    // метод проверки пользовательского ввода на валидность
    public static void checkInput(RemoteController remoteController, int bounder, int input) {
        if (input > 0 && input <= bounder) {
            System.out.println(remoteController.switchChannel(input).getProgramName());
        } else {
            System.out.println("Your input is out of available channel range (1-" + bounder + ")," +
                    " or there is currently no program for this channel");
        }
    }
}
