public class Program {
    private final String programName;

    public Program(String programName) {
        this.programName = programName;
    }

    public String getProgramName() {
        return "current program: " + programName;
    }
}
