public class RemoteController {
    private TV tv;

    // передача объекта "ТВ"
    public void setTv(TV tv) {
        this.tv = tv;
    }

    // метод переключения на канал телевизора
    public Program switchChannel(int input) {
        return tv.switchChannel(input);
    }
}
