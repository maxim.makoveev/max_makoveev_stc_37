import java.util.Random;

public class Channel {
    private Program[] programs;
    private final String channelName;

    public Channel(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelName() {
        return "Switched to Channel " + channelName + ", ";
    }

    public void setPrograms(Program[] programs) {
        this.programs = programs;
    }


    // возврат случайной программы из массива программ
    public Program showRandomizedProgram() {
        Random random = new Random();
        return programs[random.nextInt(programs.length)];
    }
}
