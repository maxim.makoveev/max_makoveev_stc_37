import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // создание и инициализация матрицы каналов
        Program[][] programs = {
                {new Program("Program-1"), new Program("Program-2")},
                {new Program("Program-3"), new Program("Program-4"), new Program("Program-5")},
                {new Program("Program-33"), new Program("Program-44"), new Program("Program-55")},
                {new Program("Program-999"), new Program("Program-42"), new Program("Program-52")},
                {new Program("Program-3111"), new Program("Program-4111"), new Program("Program-5111")},
                {new Program("Program-6"), new Program("Program-7"), new Program("Program-8"), new Program("Program-9")},
                {new Program("Program-10"), new Program("Program-11"), new Program("Program-12")}
        };
        // создание и инициализация массива каналов
        Channel[] channels = {
                new Channel("Первый"), new Channel("Второй"),
                new Channel("Третий"), new Channel("Четвертый"), new Channel("Пятый")
        };

        // инициализация ТВ и Пульта
        TV tv = new TV(channels);
        RemoteController remoteController = new RemoteController();
        // подключение пульта для переключения канала
        tv.connectRemote(remoteController);

        // сканер для пользовательского ввода канала
        Scanner scanner = new Scanner(System.in);

        // решается вопрос c NullPointerException
        int bounder = Math.min(channels.length, programs.length);

        // подключение массива программ к каналам (не выходя за рамки обоих массивов)
        for (int i = 0; i < bounder; i++) {
            channels[i].setPrograms(programs[i]);
        }

        int input = scanner.nextInt();
        TV.checkInput(remoteController, bounder, input);
    }
}

