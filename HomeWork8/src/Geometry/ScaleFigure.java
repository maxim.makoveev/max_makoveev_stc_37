package Geometry;

public interface ScaleFigure {
    void scale(double ratio);
}
