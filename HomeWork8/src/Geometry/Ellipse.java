package Geometry;

public class Ellipse extends Circle {
    private final double littleAxis;


    public Ellipse(int initialX, int initialY, double bigAxis, double littleAxis) {
        super(initialX, initialY, bigAxis);
        this.littleAxis = littleAxis;
    }

    public double getLittleAxis() {
        return littleAxis;
    }

    @Override
    public double getMathPerimeter() {
        return Math.round(4 * (Math.PI * getRadius() * getLittleAxis() + Math.pow(getRadius() - getLittleAxis(), 2)) /
                (getLittleAxis() + getRadius()));
    }

    @Override
    public double getMathArea() {
        return Math.round(Math.PI * getRadius() * getLittleAxis());
    }
}
