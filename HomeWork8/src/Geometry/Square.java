package Geometry;

public class Square extends Circle {
    double sideX;

    public Square(int initialX, int initialY, double sideX) {
        super(initialX, initialY, sideX);
    }

    public double getSideX() {
        sideX = radius;
        return sideX;
    }

    @Override
    public double getMathPerimeter() {
        return 4 * getSideX();
    }

    @Override
    public double getMathArea() {
        return Math.pow(getSideX(), 2);
    }
}
