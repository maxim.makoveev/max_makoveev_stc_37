package Geometry;

public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(3, 5, 15);
        circle.changeCoordinates(6, 2);
        circle.scale(1.8);
        System.out.println("периметр равен: " + circle.getMathPerimeter());
        System.out.println("площадь равна: " + circle.getMathArea());
        System.out.println("_____");
        Ellipse ellipse = new Ellipse(21, 7, 24, 12);
        ellipse.scale(2.2);
        ellipse.changeCoordinates(7, 7);
        System.out.println("периметр равен: " + ellipse.getMathPerimeter());
        System.out.println("площадь равна: " + ellipse.getMathArea());
        System.out.println("_____");
        Square square = new Square(-2, 8, 13);
        square.changeCoordinates(1, 1);
        square.scale(5);
        System.out.println("периметр равен: " + square.getMathPerimeter());
        System.out.println("площадь равна: " + square.getMathArea());
        System.out.println("_____");
        Rectangle rectangle = new Rectangle(36, 6, 6, 16);
        rectangle.changeCoordinates(4, 2);
        rectangle.scale(11);
        System.out.println("площадь равна: " + rectangle.getMathArea());
        System.out.println("периметр равен: " + rectangle.getMathPerimeter());
    }
}
