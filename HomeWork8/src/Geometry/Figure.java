package Geometry;

public abstract class Figure implements ScaleFigure, MoveFigure {
    int initialX, initialY;

    public Figure(int initialX, int initialY) {
        this.initialX = initialX;
        this.initialY = initialY;
        System.out.println(getClass());
    }

    public abstract double getMathPerimeter();

    public abstract double getMathArea();
}
