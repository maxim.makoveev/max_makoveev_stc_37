package Geometry;

public class Rectangle extends Square {
    private double sideY;


    public Rectangle(int initialX, int initialY, double sideX, double sideY) {
        super(initialX, initialY, sideX);
        this.sideY = sideY;
    }

    public double getSideY() {
        return sideY;
    }

    @Override
    public double getMathPerimeter() {
        return 2 * (getSideX() + getSideY());
    }

    @Override
    public double getMathArea() {
        return getSideX() * getSideY();
    }

    @Override
    public void scale(double ratio) {
        radius *= ratio;
        sideY *= ratio;
    }
}

