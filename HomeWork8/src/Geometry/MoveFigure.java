package Geometry;

public interface MoveFigure {
    void changeCoordinates(int dX, int dY);
}
