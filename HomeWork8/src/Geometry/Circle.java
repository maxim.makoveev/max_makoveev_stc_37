package Geometry;

public class Circle extends Figure {
    double radius;

    public Circle(int initialX, int initialY, double radius) {
        super(initialX, initialY);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public double getMathPerimeter() {
        return Math.round(2 * Math.PI * radius);
    }

    @Override
    public double getMathArea() {
        return Math.round(Math.PI * Math.pow(radius, 2));
    }

    @Override
    public void changeCoordinates(int dX, int dY) {
        this.initialX += dX;
        this.initialY += dY;
        System.out.print("new coordinates (x,y): (" + initialX);
        System.out.println("," + initialY+ ")");
    }

    @Override
    public void scale(double ratio) {
        radius *= ratio;
    }
}
